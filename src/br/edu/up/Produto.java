package br.edu.up;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Produto implements Serializable{
	
	private static final long serialVersionUID = -4736108166091497768L;
	
	private Long id;
	private String nome;
	private Double valor;
	
	//Getters e Setters.
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return "ID: " + id + ", Nome: " + nome;
	}

}